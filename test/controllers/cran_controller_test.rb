require 'test_helper'

class CranControllerTest < ActionDispatch::IntegrationTest
  test "cran_data endpoints returns package data" do
    get '/cran_data'
    response = JSON.parse(@response.body)
    assert_not_empty response['packages']
    assert_equal 'MyString', response['packages'][0]['name']
  end
end
