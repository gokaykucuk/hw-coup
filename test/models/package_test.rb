require 'test_helper'

class PackageTest < ActiveSupport::TestCase
  test "package model can handle updating correct packages and adding versions" do
    Package.process_update({"Package"=>'Test',"Version"=>'1.0.0'})
    assert 1, Package.find_by(name:'Test').versions.length
    Package.process_update({'Package'=>'Test','Version'=>'2.0.0'})
    assert 2, Package.find_by(name:'Test').versions.length
  end
end
