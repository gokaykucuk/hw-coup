require 'test_helper'

class CranUpdaterJobTest < ActiveJob::TestCase
  test "Cran updater can pull PACKAGES and create jobs" do
    CranUpdaterJob.perform
    assert Resque.info[:pending] > 0
  end
end
