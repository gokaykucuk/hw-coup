require 'test_helper'

class PackageUpdaterJobTest < ActiveJob::TestCase
  test 'PackageUpdaterJob can extract description for given package name and version' do
    PackageUpdaterJob.perform('A3','1.0.0')
    assert_not_nil Package.find_by(name:'A3')
  end
end
