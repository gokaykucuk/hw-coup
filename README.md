# README

## Installation

Standard Rails installation, I've used Ruby 2.5.1 but it should be ok with 2.4.4 too. 
After running 'bundle install', you also need the following,

- Postgresql server (defaults can be found and configured in database.yml)
- Redis instance (defaults to localhost:6379)

### IMPORTANT
It's assumed the system will run on linux, **curl** and **tar** tools are required.
Dont' forget to run migrations, otherwise React frontend will generate weird errors.

# First Run

On the first run, to fill the database intiially, run the follwing rake task
**while having at least 1 worker** which consumes the resulting PackageUpdateJobs.

- rake cran:pull

## React

In this application I tried to make the frontend with React. To achive this,
the project converted to a **Rails API** project, app/views folder removed, instead there
is a React application living in **app/view**. When built react application will deploy itself
into /public folder.

## Workers

The system that pulls cran packages and processes them with **Resque and Scheduler**. Deployment
must run at least one worker and one scheduler.

- rake resque:scheduler
- QUEUE=* rake resque:work

Scheduler will push **CranUpdaterJob at 12.00 pm**(hopefully, it's not super clear when it comes 
to, 12.00 am/pm status). **CranUpdaterJob** will pull the **PACKAGES** file from cran repo, and then 
push jobs to Redis instance for other workers to consume. **PackageUpdaterJob** workers will pick those up
and individually process one package.

## View

The view is a single page application. It pulls all the information about packages on boot, and then the search
functionality runs on client side.
