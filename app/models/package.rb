class Package < ApplicationRecord
    def self.process_update package_info
        # Sample package_info
        # ===================
        # "Package"=>"A3", 
        # "Type"=>"Package", 
        # "Title"=>"Accurate, Adaptable, and Accessible Error Metrics for Predictive Models", 
        # "Version"=>"1.0.0", 
        # "Date"=>"2015-08-15", 
        # "Author"=>"Scott Fortmann-Roe", 
        # "Maintainer"=>"Scott Fortmann-Roe <scottfr@berkeley.edu>", 
        # "Description"=>"Supplies tools for tabulating and analyzing the results of predictive models. The methods employed are applicable to virtually any predictive model and make comparisons between different methodologies straightforward.",
        # "License"=>"GPL (>= 2)", 
        # "Depends"=>"R (>= 2.15.0), xtable, pbapply", 
        # "Suggests"=>"randomForest, e1071", 
        # "NeedsCompilation"=>"no", 
        # "Packaged"=>"2015-08-16 14:17:33 UTC; scott", 
        # "Repository"=>"CRAN", 
        # "Date/Publication"=>"2015-08-16 23:05:52"

        # Try to handle array requests with 1 object
        if package_info.class == Array && package_info.length == 1
            package_info = package_info[0]
        end

        # Try finding or crating package from name and update properties
        
        package = Package.find_or_create_by(name:package_info['Package'])
        package.title = package_info['Title']
        package.description = package_info['Description']
        package.pub_date = package_info['Date']
        # Look up the versions, if this one is missing, push it.
        package.versions << package_info['Version']
        # Remove the version name if it's already there
        package.versions.uniq!
        
        # Process authors and maintainers
        package.maintainers = package_info['Maintainer']
        package.authors = package_info['Author']
        package.save
    end
end
