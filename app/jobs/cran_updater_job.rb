class CranUpdaterJob < ApplicationJob
  @queue = :cran_updater

  def self.perform
    # First cleanup all the files from temporary storage folder
    FileUtils.rm_rf(Dir.glob(CRAN_STORAGE_FOLDER+'*'))

    # Get the new packages file
    begin
      packages = Dcf.parse(RestClient.get(CRAN_BASE_URL+'PACKAGES').body)
      # {"Package"=>"A3",
      #  "Version"=>"1.0.0",
      #  "Depends"=>"R (>= 2.15.0), xtable, pbapply",
      #  "Suggests"=>"randomForest, e1071",
      #  "License"=>"GPL (>= 2)",
      #  "NeedsCompilation"=>"no"}
      packages.each do |package|
        Resque.enqueue(PackageUpdaterJob, package['Package'], package['Version'])
      end
    rescue StandardError => e
      puts e
      # TODO: Add a simple alert system like email sending
    end

  end
end
