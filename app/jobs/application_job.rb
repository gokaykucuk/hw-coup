require 'dcf'
class ApplicationJob
    # Constants for cran links
    CRAN_BASE_URL = 'https://cran.r-project.org/src/contrib/'
    CRAN_STORAGE_FOLDER = Rails.root.join('tmp','cran')
end
