class PackageUpdaterJob < ApplicationJob
  @queue = :package_updater

  def self.perform(package_name,package_version)
    remote_filename = package_name+'_'+package_version+'.tar.gz'
    # Download the package to temporary cran folder.
    system("curl -s #{CRAN_BASE_URL + remote_filename} | tar xvz -C #{CRAN_STORAGE_FOLDER} #{package_name}/DESCRIPTION")
    # Read back downloaded DESCRIPTION file
    description = Dcf.parse(File.read("#{CRAN_STORAGE_FOLDER}/#{package_name}/DESCRIPTION"))
    Package.process_update description
  end
end
