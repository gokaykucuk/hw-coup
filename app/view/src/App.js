import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: [],
      items_to_render: [] //Items to render is the real rendered array
    };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    const { items } = this.state;
    let search_param = event.target.value;
    // Filter out the unwanted items.
    let filtered_items = items.filter(function(item){
      return item.name.includes(search_param) || item.title.includes(search_param) || item.description.includes(search_param);
    });
    this.setState({items_to_render:filtered_items});
  }
  componentDidMount() {
    // During development, change this to localhost:3000/cran_data otherwise it'll try to get data from react server
    fetch("http://localhost:3000/cran_data")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            items: result.packages,
            items_to_render: result.packages
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }
  render() {
    const { error, isLoaded, items, items_to_render } = this.state;
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to Cran Search</h1>
        </header>
        <p className="App-intro">
        {
          error ?
          <h1>{error.message}</h1>
          :
          <input className='Search-bar' type='text' onChange={this.handleChange}/>
        }
        </p>
        <ul>
          {items_to_render.map(item => (
            <li key={item.name}>
              {item.name}
              {item.versions.map(version => (
                <a href={"https://cran.r-project.org/src/contrib/"+item.name+"_"+version+".tar.gz"}>
                 ,({version})
                </a>
              ))}
              
            </li>
          ))}
        </ul>
      </div>      
    );
  }
}

export default App;
