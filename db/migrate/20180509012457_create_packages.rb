class CreatePackages < ActiveRecord::Migration[5.2]
  def change
    create_table :packages do |t|
      t.string :name
      t.string :title
      t.date :pub_date
      t.text :description
      t.jsonb :versions, default:[]
      t.string :maintainers
      t.text :authors
      t.timestamps
    end
  end
end
